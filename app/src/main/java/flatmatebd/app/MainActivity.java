package flatmatebd.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.app.ProgressDialog;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class MainActivity extends AppCompatActivity {

    private WebView webview;
    ProgressDialog progressDialog;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        setContentView(R.layout.activity_main);
        progressDialog = ProgressDialog.show(this, "Please Wait", "Loading...", true);

        webview =(WebView)findViewById(R.id.webView);


        webview.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {


                if (url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("MobileApp")
                            .setAction("ClickedMobileNumber")
                            .build());
                    view.getContext().startActivity(intent);
                    return true;
                }

                if (Uri.parse(url).getHost().endsWith("flatmate.com.bd")) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("MobileApp")
                            .setAction("OpenLink")
                            .build());
                    return false;
                }

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if(!progressDialog.isShowing()){
                    progressDialog.show();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if(progressDialog.isShowing() && progressDialog!=null)
                {
                    progressDialog.dismiss();
                }
            }
        });
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.setOverScrollMode(WebView.OVER_SCROLL_NEVER);

        webview.loadUrl("https://flatmate.com.bd");
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("MobileApp")
                .setAction("OpenedApp")
                .build());
    }

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            super.onBackPressed();
        }
    }

}
